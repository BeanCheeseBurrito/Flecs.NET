#if Cpp_Entities_Hierarchy

using Flecs.NET.Core;

void IterateTree(Entity e, Position pParent = default)
{
    // Print hierarchical name of entity & the entity type
    Console.WriteLine($"{e.Path()} [{e.Type().Str()}]");

    // Get entity position
    ref readonly Position p = ref e.Get<Position>();

    // Calculate actual position
    Position pActual = new() { X = p.X + pParent.X, Y = p.Y + pParent.Y };
    Console.WriteLine($"({pActual.X}, {pActual.Y})\n");

    // Iterate children recursively
    e.Children((Entity child) => IterateTree(child, pActual));
}

using World world = World.Create();

// Create a simple hierarchy.
// Hierarchies use ECS relationships and the builtin EcsChildOf relationship to
// create entities as children of other entities.

Entity sun = world.Entity("Sun")
    .Add<Star>()
    .Set(new Position { X = 1, Y = 1 });

world.Entity("Mercury")
    .ChildOf(sun) // Shortcut for Add(Ecs.ChildOf, sun)
    .Add<Planet>()
    .Set(new Position { X = 1, Y = 1 });

world.Entity("Venus")
    .ChildOf(sun)
    .Add<Planet>()
    .Set(new Position { X = 2, Y = 2 });

Entity earth = world.Entity("Earth")
    .ChildOf(sun)
    .Add<Planet>()
    .Set(new Position { X = 3, Y = 3 });

    Entity moon = world.Entity("Moon")
        .ChildOf(earth)
        .Add<Moon>()
        .Set(new Position { X = 0.1, Y = 0.1 });

// Is the Moon a child of Earth?
Console.WriteLine($"Child of Earth? {moon.IsChildOf(earth)}\n");

// Do a depth-first walk of the tree
IterateTree(sun);

public struct Position
{
    public double X { get; set; }
    public double Y { get; set; }
}

public struct Star { }
public struct Planet { }
public struct Moon { }

#endif

// Output:
// Child of Earth? True
//
// Sun [Star, Position, (Identifier,Name)]
// (1, 1)
//
// Sun.Mercury [Position, Planet, (Identifier,Name), (ChildOf,Sun)]
// (2, 2)
//
// Sun.Venus [Position, Planet, (Identifier,Name), (ChildOf,Sun)]
// (3, 3)
//
// Sun.Earth [Position, Planet, (Identifier,Name), (ChildOf,Sun)]
// (4, 4)
//
// Sun.Earth.Moon [Position, Moon, (Identifier,Name), (ChildOf,Sun.Earth)]
// (4.1, 4.1)
