﻿#if Example

Console.WriteLine("To run an example, use \"dotnet run --project Flecs.NET.Examples -p\"");
Console.WriteLine("Example: \"dotnet run --project src/Flecs.NET.Examples --property:Example=Cpp_Entities_Basics\"");

#endif
